# Copies all theme files to their folders
# Requires sudoer for two of them

# This is for QTerminal, the terminal that Kali uses. If you have Qterminal,
# then you should uncomment these two commands.
#sudo mkdir -p /usr/share/qtermwidget5/color-schemes/
#sudo cp -r ./color-scheme/* -t /usr/share/qtermwidget5/color-schemes/

# If you don't want to install the Qt5 Configuration Tool, either comment this line out, or refuse to install it
sudo pacman -S qt5ct
# sudo apt install qt5ct

sudo mkdir -p /usr/share/gtksourceview-3.0/styles/
sudo cp -r ./GTK3/* -t /usr/share/gtksourceview-3.0/styles/
mkdir -p ~/.icons/
cp -r ./Kali\ Theme/icons/* -t ~/.icons/
mkdir -p ~/.themes/
cp -r ./Kali\ Theme/themes/* -t ~/.themes/
mkdir -p ~/.config/qt5ct/colors
cp -r ./qt5ct/Kali-* -t ~/.config/qt5ct/colors/
mkdir -p ~/.config/qt5ct/qss/
cp ./qt5ct/fusion-simple-scrollbar.qss ~/.config/qt5ct/qss/
