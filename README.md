# Kali Linux Themes
These are all of the theme files found in Kali Linux (that I could find, and am aware of).  
The purpose of these is to easily style your Desktop in a similar fashion.  
## WARNING
USE AT YOUR OWN RISK.  
This is still in its testing phase, and there may be errors.
## Installation
1. Uncomment any wanted themes in `install.sh`
2. Make install.sh executable
```sh
chmod +x install.sh
```
3. Run the installer
```sh
./install.sh
```
4. Set new themes and icons to the ones in your settings manager.  
Open `qt5ct` and set color scheme to Kali-Light or Kali-Dark, and change icon theme to Flat Remix Blue. You can select the simple scrollbar option in stylesheets if you want, Kali Linux has this.
## Notes
* If you get "The application is not configured correctly." in the Qt5 Configuration Tool (qt5ct), then run `qt5ct-fix.sh` as an executable.
## TODO
* Add fonts (Cantarell/Firacode) and installation of them
* Fix "cannot overwrite directory with non-directory" error
* Fix color-schemes
* Fix Mousepad theming